#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

static const char alphabet[52] = {
	 'A', 'B', 'C', 'D',
	 'E', 'F', 'G', 'H',
	 'I', 'J', 'K', 'L',
	 'M', 'N', 'O', 'P',
	 'R', 'S', 'T', 'U',
	 'V', 'W', 'X', 'Y',
	 'Z', 'a', 'b', 'c',
	 'd', 'e', 'f', 'g',
	 'h', 'i', 'j', 'k',
	 'l', 'm', 'n', 'o',
	 'p', 'q', 'r', 's',
	 't', 'u', 'v', 'w',
	 'x', 'y', 'z' 
};

static int key = 4;
static char msg[] = "ExeuyiEksve";

int main(int argc, char **argv){
	for(int i = 0; msg[i] != '\0'; i++){
		if(msg[i] == 'A'){
			msg[i] = 'z' - key;
		}
		else{
			msg[i] -= key;
		}
	}
	puts(msg);
	
	return 0;
}
