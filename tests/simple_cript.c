/* A simple criptography algorithm */

#include <stdio.h>
#include <stdlib.h>

/* The key */
static const int key = 4;

/* The message */
static char msg[] = "Hello World!!!";

/* Length of the message */
static int msg_length = sizeof(msg) / sizeof(char);

/* Criptografando a mensagem */
static char *cript(char msg[]);

/* Decriptografando a mensagem */
static char *decript(char msg[]);

int main(int argc, char **argv){
	puts(cript(msg)); /* show cript message */

	puts(decript(msg)); /* Show decript message */
	
	return EXIT_SUCCESS;
}

static char *cript(char msg[]){
	for(int i = 0; i < msg_length; i++){
		msg[i] += key;
	}
	return msg;
}

static char *decript(char msg[]){
	for(int i = 0; i < msg_length; i++){
		msg[i] -= key;
	}
	return msg;
}

