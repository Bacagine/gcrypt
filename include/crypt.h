#ifndef _CRYPT_H
#define _CRYPT_H

#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>

/* If rot true sum character with the key, else subtract character with key */
char *caesar_cipher(const char *str, bool rot, int key);

#endif
