#ifndef _SYSTEM_H
#define _SYSTEM_H

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <libintl.h>
#include <gtk/gtk.h>

#define VERSION "21.02.10"

#define _(str) gettext(str)

/* The name of program */
extern const char *program_name;

/* Set the name of program */
static void set_program_name(const char *argv0);

/* Get the name of program */
//static const char *get_program_name(void);

/* Show the verion of the program */
static void print_version(void);

/* Show a help for user */
static void print_help(void);

#endif
