#include "../include/program.h"

static void set_program_name(const char *argv0){
    program_name = argv0;
}

static void print_version(void){
    printf("%s %s\n", program_name, VERSION);
}

static void print_help(void){
    printf(_("Usage: %s <args>\n"), program_name);
    printf(_("\
    -h, --help        display this help and exit\n\
    -v, --version     display the version and exit\n\
    --license         display the license and exit\n"));
}

