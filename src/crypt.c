#include "../include/crypt.h"

char *caesar_cipher(const char *str, bool rot, int key){
    char *str_crypt = (char *) malloc(strlen(str) * sizeof(char));
    
    if(!rot){
        for(int i = 0; str[i] != '\0'; i++){
			if(str[i] == 'z'){
				str_crypt[i] = 'A' + key;
			}
			else if(str[i] == 'Z'){
				str_crypt[i] = 'a' + key;
			}
			else{
				str_crypt[i] = str[i] + key;
			}
        }
    }
    else{
        for(int i = 0; str[i] != '\0'; i++){
			if(str[i] == 'A'){
				str_crypt[i] = 'z' - key;
			}
			else if(str[i] == 'a'){
				str_crypt[i] = 'Z' - key;
			}
			else{
            	str_crypt[i] = str[i] - key;
			}
        }
    }

    return str_crypt;
}

